# Garben und Logik

**[Übungsblätter](https://iblech.gitlab.io/garben-und-logik/uebungen.pdf)**

**[Skript](https://iblech.gitlab.io/garben-und-logik/notes.pdf)**


## Worum geht es?

**Was genau liefert ein Beweis mehr als nur die Information, dass die bewiesene
Aussage stimmt? Was hat man davon, wenn man auf das Axiom, jede Aussage sei
wahr oder falsch, verzichtet?**

In dieser Vorlesung möchten wir mathematische Beweise als eigenständige
mathematische Objekte behandeln und von einem höheren Standpunkt aus
betrachten. Das ist mit Gentzens Kalkül natürlichen Schließens möglich,
welches erlaubt, informale Argumente zu formalisieren. Damit ausgerüstet können
wir präzise untersuchen, auf welchen logischen Prinzipien gegebene Beweise
beruhen.

Wir lernen intuitionistische Logik kennen, bei der das Prinzip vom
ausgeschlossenen Dritten ("jede Aussage stimmt oder stimmt nicht") nicht
verwendet wird, und verstehen Gödels Unvollständigkeitssatz, welcher in erster
Näherung besagt, dass es wahre Aussagen gibt, die nicht beweisbar sind.

Anschließend behandeln wir Realisierbarkeitstheorie. Damit können wir eine
Verbindung zwischen Logik und Berechenbarkeitstheorie aus der theoretischen
Informatik herstellen und einsehen, in welchem Sinn gewisse Prinzipien gelten,
die in klassischer Mathematik schlichtweg falsch sind: etwa die Behauptung,
dass jede Funktion ℝ → ℝ stetig sei. Das werden wir nutzen, um *proof mining*
zu betreiben: aus gegebenen Beweisen weitere Informationen wie obere Schranken
zu extrahieren. Dies werden wir mit Anwendungen in der Analysis und Numerik
beleuchten.

Schließlich erkunden wir die interne Sprache von Topoi, mathematischen
Alternativuniversen, in denen nicht die üblichen Gesetze der Logik gelten. Wir
werden sehen, wie man diese in Geometrie und Algebra gewinnbringend einsetzen
kann. Auch der effektive Topos, der ein weiteres Bindeglied zur theoretischen
Informatik darstellt und Realisierbarkeitstheorie gewissermaßen
vergegenständlicht, wird thematisiert werden.


## Termine

**Vorlesung:** Mittwochs 14:00 Uhr und freitags 8:15 Uhr in 2004/L1

**Übung:** Montag 17:30 Uhr in 1008/L1 (mit optionaler anschließender Brotzeit)


## Aufzeichnung der Vorlesungen

Freundlicherweise zeichnet das Team vom [Videolabor der Universität
Augsburg](https://ml.phil.uni-augsburg.de/service/av-mediendienste/) die
Vorlesung auf. [Zum Kanal auf
YouTube.](https://www.youtube.com/playlist?list=PLR-3Jx6BfhkgjagoGM3Z15G0j5hv5i66v)

1. [Vorlesungsüberblick](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/V_XXX_BlechschmidtMittwoch_20171018_MASTER_abr750.mp4.zip)
2. [Gentzens Kalkül natürlichen Schließens](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/Blechschmidt_WS17-18/2017_10_20_Blechschmidt.zip)
3. [Modale Operatoren](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_10_25_blechschmidt.zip)
4. [Gentzens Beweis der Konsistenz von Peano-Arithmetik (erster Teil)](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/Blechschmidt_ws17-18/2017_10_27_Blechschmidt.zip)
5. [Gentzens Beweis der Konsistenz von Peano-Arithmetik (zweiter Teil)](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_11_03_Blechschmidt.zip)
6. Berechenbare Funktionen (leider ohne Aufzeichnung)
7. [Der Maschinenraum von Arithmetik](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_11_10_Blechschmidt.zip)
8. [Gödels Unvollständigkeitssätze (erster Teil)](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_11_15_blechschmidt.zip)
9. [Gödels Unvollständigkeitssätze (zweiter Teil)](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_11_17_Blechschmidt.zip)
10. [Gödels Vollständigkeitssatz](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_11_22_blechschmidt.zip)
11. [Gödels Vollständigkeitssatz (zweiter Teil) und Motivation für Realisierbarkeitstheorie](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_11_24_Blechschmidt.zip)
12. [Erste Schritte im Realisierbarkeitsmodell](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_11_29_blechschmidt.zip)
13. [Weitere Erkundung und Ausdehnung des Realisierbarkeitsmodells](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_12_01_Blechschmidt.zip)
14. [Erkundung der formalen Church–Turing-These](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_12_06_blechschmidt.zip)
15. [Kategorizität von Arithmetik und Superturingmaschinen](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_12_08_Blechschmidt.zip)
16. [Modifizierte Realisierbarkeit und berechenbare Analysis](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_12_13_blechschmidt.zip)
17. [Konstruktive Phänomene in der Analysis](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_12_15_Blechschmidt.zip)
18. [Konstruktiver Beweis des Zwischenwertsatzes (nach Matthew Frank) und Proof Mining in der Analysis](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_12_20_blechschmidt.zip)
19. [Mehr zur Dialectica-Interpretation](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2017_12_22_Blechschmidt.zip)
20. [Dynamische Methoden in der Algebra](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_01_10_blechschmidt.zip)
21. [Örtlichkeiten](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_01_12_Blechschmidt.zip)
22. [Konstruktionen mit Örtlichkeiten und Unterörtlichkeiten](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_01_17_blechschmidt.zip)
23. [Universelle Modelle, Siten und Garben](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_01_19_Blechschmidt.zip)
24. [Interne Sprache von Topoi](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_01_24_blechschmidt.zip)
25. [Die interne Sprache des Zariski-Topos und klassifizierende Topoi](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_01_26_Blechschmidt.zip)
26. [Geometrische Morphismen, Punkte von Topoi und Logik höherer Ordnung](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_01_31_blechschmidt.zip)
27. [Mehr Logik höherer Ordnung und das Spektrum als universelle Lokalisierung](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_02_02_Blechschmidt.zip)
28. [Parameterabhängigkeit, weitere Konstruktionen und interne Topostheorie](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_02_07_blechschmidt.zip)
29. [Interne Örtlichkeiten, interne Theorien und Rückzug längs Örtlichkeitsmorphismen](http://videolabor.phil.uni-augsburg.de/avmdii/ws17-18/blechschmidt_ws17-18/2018_02_09_Blechschmidt.zip)
30. Umarme Topologie, Idempotenz, das Brückenbauprogramm von Olivia Caramello (nur auf [YouTube](https://www.youtube.com/watch?v=q6wR3ZPK9gg&t=0s&list=PLR-3Jx6BfhkgjagoGM3Z15G0j5hv5i66v&index=30))


## Skript und Übungsblätter

Die Übungsblätter entstehen nach und nach [hier auf
GitLab](https://iblech.gitlab.io/garben-und-logik/uebungen.pdf). Anregungen sind immer
herzlich willkommen.

Gebrabbel, das vielleicht irgendwann ein Skript wird, steht [ebenfalls auf
GitLab](https://iblech.gitlab.io/garben-und-logik/notes.pdf).

Weitere Materialien stehen [ebenfalls auf
GitLab](https://gitlab.com/iblech/garben-und-logik/tree/master/stuff).


## Vorlesungsplan

In eckigen Klammern: mögliche Übungsaufgaben

1. **Grundlagen**
    * Woche 1: Vorlesungsüberblick und Gentzens Kalkül natürlichen Schließens
      *[Formalisierung von Beweisen, Brouwersche Gegenbeispiele]*
    * Woche 2: Übersetzungen und Gentzens Konsistenzbeweis von Peano-Arithmetik
      *[A-Übersetzung, Ordinalanalysis]*
    * Woche 3: Entscheidungsmethoden für gewisse formale Systeme
    * Woche 4: Gödels Vollständigkeitssatz und der Löwenheim–Skolemsche Satz
      *[Phänomene von Joel David Hamkins]*
2. **Arithmetisierung von Syntax**
    * Woche 5: Rekursive Funktionen und ihre Darstellung
    * Woche 6: Das Diagonallemma, Gödels Unvollständigkeitssatz und Konsequenzen
      *[Formalisierbarkeit von Wahrheit, Löbs Theorem, …]*
3. **Realisierbarkeitstheorie**
    * Woche 7: Einführung in die Realisierbarkeitstheorie
    * Woche 8: Konstruktive Prinzipien
      *[Spaß und Nutzen von Traumaxiomen, effektive Analysis nach Bishop/Bridges]*
    * Woche 9: *Puffer oder weitere Themen, etwa Unterschiede bei höheren Typen*
4. **Proof mining und rechnerischer Inhalt:** Was genau liefert ein Beweis mehr
   als nur die Information, dass die bewiesene Aussage stimmt?
    * Woche 10: Proof mining mittels der Realisierbarkeitsinterpretation
      *[Beweis von Metatheoremen und explizite Schrankenextraktion]*
    * Woche 11: Dynamische Methoden in der Algebra
      *[Extraktion von Zeugen]*
5. **Die interne Sprache von Topoi**
    * Woche 12: Grundlagen
    * Woche 13: Anwendungen in Algebra und Geometrie
    * Woche 14: Vollständigkeit von Garbensemantik
    * Woche 15: Der effektive Topos

Gewünscht, in diesen Plan aber noch nicht eingearbeitet:

* Kategorielle Modelle von Typentheorie
* Forcing (etwa um Unentscheidbarkeit der Kontinuumshypothese oder, vielleicht
  spannender, Unabhängigkeit des Auswahlaxioms zu zeigen)
* Diophantische Gleichungen (nur ganz kurzer Exkurs)

Kandidaten für Themen, die schneller (oder in Übungen) behandelt werden
könnten, um Zeit für diese zu schaffen:

* Entscheidungsmethoden für gewisse formale Systeme
* Rekursive Funktionen und ihre Darstellung
* Vollständigkeit von Garbensemantik

In den Übungen werden wir immer mal wieder mit Theoremumgebungen wie Coq, Agda,
Isabelle oder Nuprl arbeiten.


## Lernziele

Wir können präzisieren, auf welchen logischen Prinzipien gegebene
mathematische Argumentationen beruhen, und können Beweise formalisieren. Wir
verstehen die Beziehungen zwischen Syntax und Semantik und haben ein
reflektiertes Verständnis von Gödels Unvollständigkeitssätzen.

Weiterhin verstehen wir den Bezug von formaler Logik zur
Berechenbarkeitstheorie in der theoretischen Informatik. Wir kennen klassische
und nichtklassische Prinzipien, die in Realisierbarkeitsinterpretationen
intuitionistischer Logik gelten. Wir können in einfachen Fällen aus Beweisen
rechnerischen Inhalt wie obere Schranken extrahieren.

Wir verstehen die Grundlagen der internen Sprache von Topoi, kennen Anwendungen
in Algebra und Geometrie und können die Sprache in einfachen Situationen
selbstständig anwenden.


## Literatur

* [A. Bauer. Realizability as the connection between computable and constructive mathematics. 2015.](http://math.andrej.com/data/c2c.pdf)
* E. Bishop und D. Bridges. Constructive Analysis. Springer, 1985.
* [I. Blechschmidt. Using the internal language of toposes in algebraic geometry. 2017.](https://rawgit.com/iblech/internal-methods/master/notes.pdf)
* [M. Coste, H. Lombardi und M.-F. Roy. Dynamical method in algebra: effective Nullstellensätze. In: Ann. Pure Appl. Logic 111.3 (2001), S. 203–256.](https://perso.univ-rennes1.fr/michel.coste/publis/clr.pdf)
* H. Lombardi und C. Quitté. Commutative Algebra: Constructive Methods. Springer, 2015.
* S. Mac Lane und I. Moerdijk. Sheaves in Geometry and Logic. Springer, 1992.
* R. Mines, F. Richman und W. Ruitenburg. A Course in Constructive Algebra. Springer, 1988.
* [M. Nieper-Wißkirchen. Vorlessungsskript zur Linearen Algebra. 2009.](https://www.math.uni-augsburg.de/prof/alg/Downloads/Lineare-Algebra.pdf)
* [M. Nieper-Wißkirchen. Galoissche Theorie. 2013.](https://www.math.uni-augsburg.de/prof/alg/Downloads/Einfuehrung-in-die-Algebra.pdf)
* [Open Logic Project. The Open Logic Text. 2017.](http://builds.openlogicproject.org/open-logic-complete.pdf)
* [T. Streicher. Introduction to category theory and categorical logic. 2004.](http://www.mathematik.tu-darmstadt.de/~streicher/CTCL.pdf)
* A. Troelstra und D. van Dalen. Constructivism in Mathematics: An Introduction. North-Holland Publishing, 1988.


## Sheafification Man

![](aushang/sheafification-man.png)
