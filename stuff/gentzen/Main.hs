module Main where

import Core
import Tactics
import PrettyPrinting
import Types
import Parser

ex0 = prove (parseFormula "∀(∀(#0 = #1 ⇒ #1 = #0))") $
    arbitrary $ \a ->
        arbitrary $ \b ->
            assuming $
                const punt $
                reduceBy ((b `Eq` a) `And` (b `Eq` b))
                    (both (byAssumption 0) identity)
                    punt --byIndiscernibles

ex1 = prove (parseFormula "∀(#0 = #0)") $
    arbitrary $ \x ->
        identity

ex2 = prove (parseFormula "φ ⇒ φ") $
    assuming $
        byAssumption 0

ex3 = prove (parseFormula "∃(#0 = 0)") $
    setting Zero $
        identity

ex4 = prove (parseFormula "∀(#0 = 0 ∨ (∃(#1 = succ(#0))))") $
    byInduction
        (left identity)
        (\n -> right $ setting n $ identity)

ex5 = prove (parseFormula "forall. (0 = #0 or not (0 = #0))") $
    byInduction
        (left identity)
        (\n -> right $ weaken $ assuming $
            reduceBy (parseFormula "exists. (0 = succ(#0))")
                (setting n $ byAssumption 0) $ fromContext [] $
                    reduceBy' (parseFormula "exists. (0 = succ(#0))")
                        (weaken (byKnown axZero))
                        triv)

ex6 = prove (parseFormula "forall. (forall. (#1 = #0 or not (#1 = #0)))") $
    byInduction (suppress "ex5" <$> byKnown ex5) $ \a ->
        byInduction
            punt
            (\b -> punt)

ex7 = prove (parseFormula "forall. (+(0,#0) = #0)") $
    byInduction (with' Zero axPlusZero) $ \a ->
        reduceBy (parseFormula "succ(+(0,a)) = succ(a)")
            punt
            punt

ex8 = prove (parseFormula "¬(¬(φ ∨ ¬φ))") $
    assuming $
        reduceBy (parseFormula "φ ∧ ¬φ")
            (both
                punt
                punt)
            (reduceBy' (parseFormula "φ") (byAssumption 0) (byAssumption 1))

main :: IO ()
main = putStrLn $ toTeX ex4
