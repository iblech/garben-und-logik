module Tactics where

import Debug.Trace
import Control.Monad.Reader
import qualified Data.Map as M

import Types
import Core

type M a = Reader (Context,Formula,Formula) a

prove :: Formula -> M Proof -> Proof
prove delta = proveSequent ([],Top,delta)

proveSequent :: (Context,Formula,Formula) -> M Proof -> Proof
proveSequent (c,gamma,delta) f
    | p <- runReader f (c,gamma,delta)
    , antecedent (claim p) == gamma
    , consequent (claim p) == delta
    = p
    | otherwise
    = undefined

conciseCut :: Proof -> Proof -> Proof
conciseCut p q
    | antecedent (claim p) == consequent (claim p) = q
    | antecedent (claim q) == consequent (claim q) = p
    | otherwise                                    = cut p q

both :: M Proof -> M Proof -> M Proof
both f g = do
    (c,gamma,And phi psi) <- ask
    let p = proveSequent (c,gamma,phi) f
    let q = proveSequent (c,gamma,psi) g
    return $ conjunctionI p q

left :: M Proof -> M Proof
left f = do
    (c,gamma,Or delta phi) <- ask
    let p = proveSequent (c,gamma,delta) f
    return $ conciseCut p (disjunctionIleft c delta phi)

right :: M Proof -> M Proof
right f = do
    (c,gamma,Or phi delta) <- ask
    let p = proveSequent (c,gamma,delta) f
    return $ conciseCut p (disjunctionIright c phi delta)

assuming :: M Proof -> M Proof
assuming f = do
    (c,gamma,Implies phi delta) <- ask
    return . impliesI $ proveSequent (c, And gamma phi, delta) f

byAssumption :: Nat -> M Proof
byAssumption 0 = do
    (c,gamma,delta) <- ask
    if gamma == delta then triv else do
    case gamma of
        And phi psi | psi == delta ->
            return $ conjunctionEright c (And phi psi)
        otherwise ->
            undefined
byAssumption n = do
    (c,And phi psi,delta) <- ask
    return $ conciseCut
        (conjunctionEleft c (And phi psi))
        (proveSequent (c,phi,delta) (byAssumption (n-1)))

freshName :: M Name
freshName = do
    (c,_,_) <- ask
    return $ head $ filter (`notElem` c) $ map (:"") ['a'..'z'] ++ map (("v" ++) . show) [0..]

arbitrary :: (Term -> M Proof) -> M Proof
arbitrary f = do
    (c,gamma,ForAll delta) <- ask
    name <- freshName
    let delta' = unbind (FreeVar name) delta
    return . forallI $ proveSequent (name:c,gamma,delta') (f $ FreeVar name)

setting :: Term -> M Proof -> M Proof
setting t f = do
    (c,gamma,Exists delta) <- ask
    name <- freshName
    let delta' = unbind t delta
    let p = proveSequent (c,gamma,delta') f
    return $ conciseCut p $ substitution
        (M.fromList ((name,t) : map (\x -> (x, FreeVar x)) c))
        (existsI (name:c) (unbind (FreeVar name) delta))
        c

byKnown :: Proof -> M Proof
byKnown p = do
    (c,gamma,phi) <- ask
    if c == context (claim p) && gamma == antecedent (claim p) && phi == consequent (claim p)
        then return p
        else undefined

weaken :: M Proof -> M Proof
weaken f = do
    (c,gamma,delta) <- ask
    let -- p = proveSequent ([],Top,delta) f
    -- return $ conciseCut (topI c gamma) (substitution M.empty p c)
    let p = proveSequent (c,Top,delta) f
    return $ conciseCut (topI c gamma) p

reduceByAxiom :: Proof -> M Proof -> M Proof
reduceByAxiom p f
    | c <- claim p
    , null (context c) && antecedent c == Top
    , phi <- consequent c
    =
    undefined

with' :: Term -> Proof -> M Proof
with' t p = with t (consequent (claim p)) (return p)

with :: Term -> Formula -> M Proof -> M Proof
with t (ForAll phi) f = do
    (c,gamma,delta) <- ask
    name <- freshName
    let delta' = unbind t phi
    if delta == delta'
        then do
            let p = proveSequent (c,gamma,ForAll phi) f
            return $ conciseCut p $ substitution
                (M.fromList ((name,t) : map (\x -> (x, FreeVar x)) c))
                (forallE (name:c) (unbind (FreeVar name) phi))
                c
        else undefined

identity :: M Proof
identity = do
    (c,gamma,Eq t t') <- ask
    if t == t'
        then return $ conciseCut (topI c gamma) (identityI c t)
        else undefined

triv :: M Proof
triv = do
    (c,gamma,delta) <- ask
    error $ show (gamma,delta)
    if gamma == delta
        then return $ refl c gamma
        else undefined

reduceBy :: Formula -> M Proof -> M Proof -> M Proof
reduceBy phi f g = do
    (c,gamma,delta) <- ask
    let p = proveSequent (c,gamma,phi) f
    let q = proveSequent (c,phi,delta) g
    return $ conciseCut p q

reduceBy' :: Formula -> M Proof -> M Proof -> M Proof
reduceBy' phi f g = do
    -- f : gamma |- (phi ==> delta)
    -- g : gamma |- phi
    -- überführen zu:
    --   gamma |- phi and (phi ==> delta)
    --   phi and (phi ==> delta) |- delta
    (c,gamma,delta) <- ask
    reduceBy (phi `And` (phi `Implies` delta)) (both g f) (return $ impliesE c phi delta)

fromContext :: Context -> M Proof -> M Proof
fromContext c' f = do
    (c,gamma,delta) <- ask
    if not (validInContext c' 0 gamma && validInContext c' 0 delta) then undefined else do
    if not (all (`elem` c) c') then undefined else do
    let p = proveSequent (c',gamma,delta) f
    return $ substitution (M.fromList [(v,FreeVar v) | v <- c']) p c

byInduction :: M Proof -> (Term -> M Proof) -> M Proof
byInduction f g = do
    (c,gamma,delta@(ForAll delta')) <- ask
    name <- freshName
    let base = unbind Zero delta'
    let step = ForAll $ bind name $
            unbind (FreeVar name) delta' `Implies`
            unbind (Succ (FreeVar name)) delta'
    let p = proveSequent (c,gamma,base) f
    let q = proveSequent (name:c, gamma `And` unbind (FreeVar name) delta', unbind (Succ (FreeVar name)) delta') (g $ FreeVar name)
    return $ axInductionRule name (unbind (FreeVar name) delta') p q
    {-
    reduceBy
        (And base step)
        (both f (arbitrary $ \name -> assuming (g name))) $ return $
            conciseCut
                (conjunctionI
                    (topI c (base `And` step))
                    (refl c (base `And` step)))
                (impliesE $ axInduction (name:c) (unbind (FreeVar name) delta'))
    -}

byIndiscernibles :: M Proof
byIndiscernibles = do
    (c, And (Eq (FreeVar x) (FreeVar y)) phi, psi) <- ask
    if psi == substSingle x (FreeVar y) phi
        then return $ indiscernibles x y c phi
        else undefined

punt :: M Proof
punt = do
    (c,gamma,delta) <- ask
    return $ puntI c gamma delta
