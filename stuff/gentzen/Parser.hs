module Parser where

import Text.Parser.Char
import Text.Parser.Combinators
import Text.Parser.Expression
import Text.Parser.Token
import Text.Parser.Token.Style (emptyOps)

import Text.Trifecta.Parser
import Text.Trifecta.Result

import Types
import PrettyPrinting

parseFormula :: String -> Formula
parseFormula str = case parseString (const <$> formula <*> eof) mempty str of
    Success phi -> phi
    Failure err -> error (show err)

term :: (Monad m, TokenParsing m) => m Term
term = choice
    [ symbol "0" *> pure Zero
    , do
        symbol "succ("
        t <- term
        symbol ")"
        return $ Succ t
    , choice $ flip map [("+", Plus), ("*", Mult), ("·", Mult)] $ \(s,op) -> do
        symbol $ s ++ "("
        s <- term
        symbol ","
        t <- term
        symbol ")"
        return $ op s t
    , BoundVar <$> (char '#' *> natural)
    , FreeVar  <$> token (some alphaNum)
    ]

formula :: (Monad m, TokenParsing m) => m Formula
formula = buildExpressionParser table p <?> "formula"
    where
    p = choice
        [ parens formula
        , choice (map symbol ["⊤","top"]) >> pure Top
        , choice (map symbol ["⊥","bot"]) >> pure Bot
        , try $ Eq <$> term <*> (symbol "=" *> term)
        , Atomic <$> token (some alphaNum)
        ]
    table =
        [ map (\s -> prefix s neg)               [ "¬", "-", "not" ]
        , map (\s -> binary s And     AssocLeft) [ "∧", "&", "and" ]
        , map (\s -> binary s Or      AssocLeft) [ "∨", "|", "or"  ]
        , map (\s -> binary s Implies AssocLeft) [ "⇒", "=>"       ]
        , map (\s -> binary s eqv     AssocLeft) [ "⇔", "<=>"      ]
        , map (\s -> prefix s ForAll)            [ "∀", "forall."  ]
        , map (\s -> prefix s Exists)            [ "∃", "exists."  ]
        ]
    neg phi     = Implies phi Bot
    eqv phi psi = Implies phi psi `And` Implies psi phi

binary name fun assoc = Infix (fun <$ reservedOp name) assoc
prefix name fun       = Prefix (fun <$ reservedOp name)

reservedOp name = reserve emptyOps name
