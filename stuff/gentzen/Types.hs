module Types where

type Nat  = Integer
type Name = String

data Formula
    = Top
    | And Formula Formula
    | Bot
    | Or  Formula Formula
    | Implies Formula Formula
    | ForAll Formula
    | Exists Formula
    | Eq     Term Term
    | Atomic String
    deriving (Eq)

neg :: Formula -> Formula
neg = flip Implies Bot

data Term
    = BoundVar Nat
    | FreeVar  Name
    | Zero
    | Succ Term
    | Plus Term Term
    | Mult Term Term
    deriving (Eq)

type Context = [Name]

data Sequent = MkSequent
    { context    :: Context
    , antecedent :: Formula
    , consequent :: Formula
    }
    deriving (Eq)

data Proof = MkProof
    { claim     :: Sequent
    , rule      :: String
    , subProofs :: [Proof]
    }
    deriving (Eq)
