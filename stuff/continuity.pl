#!/usr/bin/env perl

use warnings;
use strict;

# XXX: properly propagate steps to parents

sub run {
    my ($limit, $code, $arg) = @_;

    return eval("
        my \$__STEPNUM__ = 0;
        my \$__STEP__ = sub {
            \$__STEPNUM__++;
            die if $limit >= 0 and \$__STEPNUM__ > $limit;
        };
        my \$__CODE__ = \"\Q$code\E\";
        $code;
    ")->($arg);
}

sub numsteps {
    my ($code, $arg) = @_;

    return eval("
        my \$__STEPNUM__ = 0;
        my \$__STEP__ = sub {
            \$__STEPNUM__++;
        };
        my \$__CODE__ = \"\Q$code\E\";
        sub { ($code)->(\$_[0]); \$__STEPNUM__ };
    ")->($arg);
}

my $ex_f = 'sub {
    my $alpha = shift;
    $__STEP__->() for 1..8;
    return run(-1, $alpha, 3) + run(-1, $alpha, 5);
}';

my $ex_alpha = 'sub {
    my $n = shift;
    $__STEP__->() for 1..$n;

    return $n;
}';

sub modulus {
    my ($f, $alpha) = @_;

    my $s = "sub {
        my \$n = shift;
        \$__STEP__->() for 1..\$n;

        my \$x = eval { run(\$n, \"\Q$f\E\", \$__CODE__) };
        if(\$@) {
            return run(-1, \"\Q$alpha\E\", \$n);
        } else {
            if(\$x == run(-1, \"\Q$f\E\", \"\Q$alpha\E\")) {
                print(\"ui\n\");
            } else {
                return run(-1, \"\Q$alpha\E\", \$n);
            }
        }
    }";

    return numsteps($f, $s);
}

print modulus($ex_f, $ex_alpha);
